## Synopsis

This project ranks Canadian Provinces by their happiness, derived from sentiment analysis on location based tweets.
 

## Motivation

This project is a side effort for fun. Twitter streaming API is fun, and so is ongoing analysis of happiness!

## Installation

Project will build via Spring Boot's mvn spring-boot:run command. Note that a file client.keys must exist 
in your class path, and contain the Twitter API client's credentials.

## Tests

Run tests via mvn clean test command

