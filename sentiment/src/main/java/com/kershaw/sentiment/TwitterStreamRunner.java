package com.kershaw.sentiment;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;

/**
 * Runs a TwitterStream & Query
 * 
 * @author dkershaw
 *
 */
public class TwitterStreamRunner implements Runnable {

	private TwitterStream stream;
	private FilterQuery query;

	public TwitterStreamRunner(TwitterStream stream, FilterQuery query) {
		this.stream = stream;
		this.query = query;
	}

	@Override
	public void run() {
		stream.filter(query);
	}

}
