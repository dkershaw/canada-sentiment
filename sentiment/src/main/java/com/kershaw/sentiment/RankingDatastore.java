package com.kershaw.sentiment;

/**
 * Persistence Interface for Rankings.
 * 
 * @author dkershaw
 *
 */
public interface RankingDatastore {

	/**
	 * Get Rankings from persistence
	 * 
	 * @return current rankings
	 */
	Rankings getRankings();

	/**
	 * Add a new province, or new value for existing province
	 * 
	 * @param province
	 * @param sentiment 
	 * @return average
	 */
	Double addProvince(Province province, Integer sentiment);

}
