package com.kershaw.sentiment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

public class CanadianTwitterStatusListener implements StatusListener {

	private static Logger LOG = LoggerFactory.getLogger(CanadianTwitterStatusListener.class);
	private static final Integer INVALID = -1;

	private SentimentCalculator scorer;
	private RankingDatastore datastore;

	public CanadianTwitterStatusListener(SentimentCalculator scorer, RankingDatastore datastore) {
		this.scorer = scorer;
		this.datastore = datastore;
	}

	public void onStatus(Status msg) {

		if (msg.getPlace() == null || msg.getPlace().getCountry() == null) {
			LOG.debug("Not processing as status does not contain location information");
			return;
		}

		String country = msg.getPlace().getCountry();

		if (!country.equals("Canada")) {
			LOG.debug("Not processing as status originates from " + country);
			return;
		}

		// Parses Twitter Geo String for province (e.g. Halifax, Nova Scotia)
		String fullName = msg.getPlace().getFullName();
		String[] splits = fullName.split(", ");
		String provinceStr = splits[splits.length - 1];

		Province province = Province.fromString(provinceStr);
		if (province == null) {
			LOG.debug("Not processing as status originates from " + province);
			return;
		}

		int sentiment = scorer.calulate(msg.getText());
		LOG.debug(province + " : (SCORE: " + sentiment + ")");

		if (sentiment == INVALID) {
			LOG.debug("Not processing messge sentiment could not be determined");
			return;
		}

		double avg = datastore.addProvince(province, sentiment);
		LOG.debug(province + " Average = " + avg);
	}

	/**
	 * From documentation: "Called upon deletionNotice notices. Clients are
	 * urged to honor deletionNotice requests and discard deleted statuses
	 * immediately. At times, status deletionNotice messages may arrive before
	 * the status. Even in this case, the late arriving status should be deleted
	 * from your backing store."
	 * 
	 * Not storing any geolocation permanently, so no need to implement
	 */
	public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
		LOG.trace("onDeletionNotice called, ignored");
	}

	/**
	 * From documentation: "This notice will be sent each time a limited stream
	 * becomes unlimited. If this number is high and or rapidly increasing, it
	 * is an indication that your predicate is too broad, and you should
	 * consider a predicate with higher selectivity."
	 * 
	 */
	public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
		LOG.info("Number of limited status: " + numberOfLimitedStatuses);
	}

	public void onException(Exception ex) {
		LOG.error(ex.getMessage(), ex);
	}

	@Override
	/**
	 * From documentation: "Called upon location deletion messages. Clients are
	 * urged to honor deletion requests and remove appropriate geolocation
	 * information from both the display and your backing store immediately.
	 * Note that in some cases the location deletion message may arrive before a
	 * tweet that lies within the deletion range arrives. You should still strip
	 * the location data."
	 * 
	 * Not storing any geolocation permanently, so no need to implement
	 * 
	 */
	public void onScrubGeo(long arg0, long arg1) {
		LOG.trace("onScrubGeo called, ignored");
	}

	/**
	 * Called when receiving stall warnings.
	 */
	@Override
	public void onStallWarning(StallWarning arg0) {
		LOG.warn("Stall Warning called: Code: " + arg0.getCode() + " Message: " + arg0.getMessage() + " % Full:"
				+ arg0.getPercentFull());

	}
}
