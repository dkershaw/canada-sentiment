package com.kershaw.sentiment;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class RankingInMemDatastore implements RankingDatastore {

	private Rankings rankings;
	private Map<Province, AtomicInteger> counts;

	public RankingInMemDatastore() {
		this.rankings = new Rankings();
		this.counts = new HashMap<Province, AtomicInteger>();
		initializeInMemoryState();
	}

	@Override
	public Rankings getRankings() {
		return rankings;
	}

	@Override
	public Double addProvince(Province province, Integer score) {
		int count = counts.get(province).incrementAndGet();
		double currentAvg = rankings.getAverage(province);
		double newAvg = ((currentAvg * (count - 1)) + score) / count;
		rankings.putAverage(province, newAvg);
		return newAvg;
	}

	private void initializeInMemoryState() {
		for (Province p : Province.values()) {
			counts.put(p, new AtomicInteger());
		}
	}
}
