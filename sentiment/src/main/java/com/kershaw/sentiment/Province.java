package com.kershaw.sentiment;

/**
 * Generalized enumeration for Canadian Provinces.
 * 
 * @author dkershaw
 *
 */
public enum Province {

	ALBERTA("Alberta"), 
	BRITISH_COLUMBIA("British Columbia"), 
	MANITOBA("Manitoba"), 
	NEW_BRUNSWICK("New Brunswick"), 
	NEWFOUNDLAND_AND_LABRADOR("Newfoundland and Labrador"), 
	NOVA_SCOTIA("Nova Scotia"), 
	ONTARIO("Ontario"), 
	PRINCE_EDWARD_ISLAND("Prince Edward Island"), 
	QUEBEC("Québec"), 
	SASKATCHEWAN("Saskatchewan");

	private final String provinceName;

	private Province(String name) {
		this.provinceName = name;
	}

	public String getName() {
		return provinceName;
	}

	/**
	 * Given a name value, attempt to derive a Province
	 * @param name
	 * @return null if Province cannot be derived
	 */
	public static Province fromString(String name) {
		for (Province p : values()) {
			if (p.getName().equals(name)) {
				return p;
			}
		}
		return null;
	}

}
