package com.kershaw.sentiment;

/**
 * Generalization of a sentiment calculator
 * 
 * @author dkershaw
 *
 */
public interface SentimentCalculator {

	/**
	 * Calculates a string's contents for sentiment
	 * 
	 * @param str
	 * @return int sentiment score (-1 if not parsable)
	 */
	int calulate(String str);

}
