package com.kershaw.sentiment;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

/**
 * Stanford NLP Sentiment Calculator
 * 
 * Scores input string on scale 1 (Most Negative) - 4 (Most Positive)
 * 
 * @see {@link https://stanfordnlp.github.io/CoreNLP/}
 * @author dkershaw
 *
 */
public class StanfordNLPSentimentCalculator implements SentimentCalculator {

	private StanfordCoreNLP pipeline = new StanfordCoreNLP("nlp.properties");

	@Override
	public int calulate(String input) {
		int mainSentiment = 0;
		if (input == null || input.length() == 0) {
			return -1;
		}
		int longest = 0;
		Annotation annotation = pipeline.process(input);
		for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
			Tree tree = sentence.get(SentimentAnnotatedTree.class);
			int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
			String partText = sentence.toString();
			if (partText.length() > longest) {
				mainSentiment = sentiment;
				longest = partText.length();
			}

		}
		return mainSentiment;
	}

}
