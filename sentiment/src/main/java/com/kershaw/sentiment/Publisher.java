package com.kershaw.sentiment;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This class publishes (logs) information periodically, including rankings and
 * status updates
 * 
 * @author dkershaw
 *
 */
@Component
public class Publisher {

	private static final Logger log = LoggerFactory.getLogger(Publisher.class);

	private static final int PUBLISH_RATE_IN_MILLIS = 5000;
	private static final int PUBLISH_ALL_RATE_IN_MILLIS = 50000;

	@Autowired
	private RankingDatastore ds;

	@Scheduled(fixedRate = PUBLISH_RATE_IN_MILLIS)
	public void publish() {
		Rankings rankings = ds.getRankings();
		Map<String, String> rankingsMap = rankings.toMap();
		Iterator<String> provinces = rankingsMap.keySet().iterator();

		log.info(provinces.next() + " is currently the happest province, followed by " + provinces.next() + " and "
				+ provinces.next());
	}

	@Scheduled(fixedRate = PUBLISH_ALL_RATE_IN_MILLIS)
	public void publishAll() {
		Rankings rankings = ds.getRankings();
		log.info(Arrays.toString(rankings.toMap().entrySet().toArray()));
	}
}
