package com.kershaw.sentiment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import twitter4j.FilterQuery;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

@Configuration
@PropertySource("classpath:client.keys")
/**
 * Configuration (DI Container) for application.
 * Requires a file "client.keys" on the class path which contains 
 * twitter application credentials
 * 
 * @author dkershaw
 *
 */
public class TwitterClientConfiguration {

	private static Logger LOG = LoggerFactory.getLogger(TwitterClientConfiguration.class);

	@Value("${client.consumerkey}")
	private String consumerKey;

	@Value("${client.consumersecret}")
	private String consumerSecret;

	@Value("${client.accessToken}")
	private String accessToken;

	@Value("${client.accessTokenSecret}")
	private String accessTokenSecret;

	/**
	 * Canada GEO Bounded Box
	 */
	private final double[] SOUTH_WEST = { -141.020177, 42.903105 };
	private final double[] NORTH_EAST = { -44.5617213, 60.00 };

	/**
	 * Starts the TwitterStreamConnector as an independent process via Spring
	 * Boot
	 */
	@Bean
	public CommandLineRunner connector(TaskExecutor executor) {
		return new CommandLineRunner() {
			public void run(String... args) throws Exception {
				try {
					executor.execute(new TwitterStreamRunner(client(), filter()));
				} catch (Throwable t) {
					LOG.error(t.getMessage(), t);
				}
			}
		};
	}

	/**
	 * Required for local property support
	 * 
	 * @see {@link https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/support/PropertySourcesPlaceholderConfigurer.html}
	 *
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/**
	 * Required to execute the runnables within spring boot
	 * 
	 * @see {@link http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/task/SimpleAsyncTaskExecutor.html}
	 */
	@Bean
	public TaskExecutor taskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}

	private TwitterStream client() {
		TwitterStream ts = new TwitterStreamFactory(configuration()).getInstance();
		ts.addListener(listener());
		return ts;
	}

	private FilterQuery filter() {
		FilterQuery filter = new FilterQuery();
		filter.locations(new double[][] { SOUTH_WEST, NORTH_EAST });
		return filter;
	}

	private twitter4j.conf.Configuration configuration() {
		return new ConfigurationBuilder().
				setOAuthConsumerKey(consumerKey).
				setOAuthConsumerSecret(consumerSecret).
				setOAuthAccessToken(accessToken).
				setOAuthAccessTokenSecret(accessTokenSecret).
				build();
	}

	private StatusListener listener() {
		return new CanadianTwitterStatusListener(scorer(), dataStore());
	}

	private SentimentCalculator scorer() {
		return new StanfordNLPSentimentCalculator();
	}

	@Bean
	public RankingDatastore dataStore() {
		return new RankingInMemDatastore();
	}

}
