package com.kershaw.sentiment;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Rankings domain object for storing provinces / sentiment average
 * 
 * @author dkershaw
 *
 */
public class Rankings {

	private Map<Province, Double> rankings;

	public Rankings() {
		this.rankings = new LinkedHashMap<>();
		for (Province p : Province.values()) {
			rankings.put(p, 0.0);
		}
	}

	public Double getAverage(Province province) {
		return rankings.get(province);
	}

	public Double putAverage(Province province, Double average) {
		return rankings.put(province, average);
	}

	public String toString() {
		return sortByValue(rankings).toString();
	}

	public Map<String, String> toMap() {
		Map<String, String> map = new HashMap<String, String>();
		for (Province p : rankings.keySet()) {
			map.put(p.getName(), rankings.get(p).toString());
		}
		return sortByValue(map);
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		return map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

}
